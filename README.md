Delete the build directory and any previously generated build outputs.
```
./gradlew clean
```

Runs all unit tests in your project.
```
./gradlew test
```

Build debug mode
```
./gradlew assembleDebug
```

Build release mode
```
./gradlew assembleRelease
```

